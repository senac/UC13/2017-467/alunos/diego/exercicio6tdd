/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
 /*
 6- Crie um programa que determine se um dado aluno esta: aprovado , em recuperação
ou aprovado para os seguintes critérios abaixo :
Nota >7 -> aprovado , Nota entre 4 e 6 recuperação abaixo de 4 reprovado.

 */
package br.com.senac.model;

public class AlunoModel {

    public static final String APROVADO = "APROVADO";
    public static final String RECUPERACAO = "RECUPERAÇÃO";
    public static final String REPROVADO = "REPROVADO";
    
    public static String situacaoAluno(int nota) {

        if (nota >= 7) {

            return AlunoModel.APROVADO;

        } else if (nota >= 4 && nota <= 6) {

            return AlunoModel.RECUPERACAO;

        } else {

            return AlunoModel.REPROVADO;

        }

    }

}
