
package br.com.senac.test;
        
import br.com.senac.model.AlunoModel;
import org.junit.Assert;
import org.junit.Test;


public class AlunoTest {
    
    public AlunoTest() {
    }
 
    @Test
    public void deveSerReprovadoSeANotaForMenorQue4(){
        
        int notaAluno = 3;
        
        String result = AlunoModel.situacaoAluno(notaAluno);
        
        Assert.assertEquals(result, AlunoModel.REPROVADO);
    
    }
     
    @Test
    public void deveEstarEmRecuperacaoSeANotaForEntre4e6(){
        
        int notaAluno = 5;
        
        String result = AlunoModel.situacaoAluno(notaAluno);
        
        Assert.assertEquals(result, AlunoModel.RECUPERACAO);
    }
    
    @Test
    public void deveEstarAprovadoSeANotaForMaiorQue7() {
        
        int notaAluno = 8;
        
        String result = AlunoModel.situacaoAluno(notaAluno);
        
        Assert.assertEquals(result, AlunoModel.APROVADO);
    }
    
}
